﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using smc.api.instagram.Model;
using smc.Model.Connections;
using smc.tests.Integration.Exceptions;

namespace smc.tests.Integration
{
	[TestFixture]
	public class InstagramConnectionTests
	{
		private InstagramConnection _connection;

		[SetUp]
		public void SetUp()
		{
			_connection = new InstagramConnection();
		}

		[Test]
		[Ignore("Integration test")]
		public void ShouldSampleData()
		{
			var action = new Action<IEnumerable<object>>(objects =>
			{
				var posts = objects.Select(x => x as InstagramMedia);
				foreach (var post in posts.Where(x => x != null))
				{
					Console.WriteLine($"Found post with id {post.Id} from {post.User.Username}");
				}

				throw new TestFinishedException();
			});

			Assert.Throws<TestFinishedException>(() => _connection.Fetch("backtothefuture", action));
		}

		[Test]
		[Ignore("Integration test")]
		public void ShouldSearchForData()
		{

		}
	}
}
