﻿using System;

namespace smc.tests.Integration.Exceptions
{
	public class TestFinishedException : Exception
	{
		public TestFinishedException() : base("Stopping test")
		{
		}
	}
}