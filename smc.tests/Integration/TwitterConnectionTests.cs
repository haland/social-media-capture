﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using smc.Model.Connections;
using smc.tests.Integration.Exceptions;
using Tweetinvi.Core.Interfaces;

namespace smc.tests.Integration
{
	[TestFixture]
	public class TwitterServiceTests
	{
		private TwitterConnection _connection;

		[SetUp]
		public void SetUp()
		{
			_connection = new TwitterConnection();
		}

		[Test]
		[Ignore("Integration test")]
		public void ShouldSearchForData()
		{
			var action = new Action<IEnumerable<object>>(objects =>
			{
				var tweets = objects.Select(x => x as ITweet);
				foreach (var tweet in tweets.Where(x => x != null))
				{
					Console.WriteLine($@"Found tweet with id {tweet.IdStr} from {tweet.CreatedBy.ScreenName}.");
				}

				throw new TestFinishedException();
			});

			Assert.Throws<TestFinishedException>(() => _connection.Fetch("backtothefuture", action));
		}
	}
}