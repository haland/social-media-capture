﻿using System.Collections.Specialized;
using System.Configuration;
using NUnit.Framework;

namespace smc.tests.Configuration
{
	public class AppConfigTests
	{
		private NameValueCollection _config;

		[SetUp]
		public void SetUp()
		{
			_config = ConfigurationManager.AppSettings;
		}

		[Test]
		public void ShouldMergeWithSecretAppSettingsAndContainTwitterSettings()
		{
			Assert.NotNull(_config["TwitterConsumerKey"]);
			Assert.NotNull(_config["TwitterConsumerSecret"]);
			Assert.NotNull(_config["TwitterAccessToken"]);
			Assert.NotNull(_config["TwitterAccessTokenSecret"]);
		}

		[Test]
		public void ShouldMergeWithSecretAppSettingsAndContainInstagramSettings()
		{
			Assert.NotNull(_config["InstagramClientId"]);
			Assert.NotNull(_config["InstagramClientSecret"]);
			Assert.NotNull(_config["InstagramReturnUrl"]);
		}

		[Test]
		public void ShouldMergeWithSecretAppSettingsAndContainFacebookSettings()
		{
			Assert.NotNull(_config["FacebookAppId"]);
			Assert.NotNull(_config["FacebookAppSecret"]);
		}

		[Test]
		public void ShouldMergeWithSecretAppSettingsAndContainFirebaseSettings()
		{
			Assert.NotNull(_config["FirebaseUrl"]);
			Assert.NotNull(_config["FirebaseNode"]);
			Assert.NotNull(_config["FirebaseSecret"]);
		}
	}
}