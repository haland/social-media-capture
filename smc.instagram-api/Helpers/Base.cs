﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace smc.api.instagram.Helpers
{
	public class Base
	{
		protected static ICache Cache;
		protected static readonly object Threadlock = new object();

		public static T DeserializeObject<T>(string json)
		{
			return JsonConvert.DeserializeObject<T>(json);
		}

		public static string SerializeObject(object value)
		{
			return JsonConvert.SerializeObject(value);
		}

		public string RequestPostToUrl(string url, NameValueCollection postData, WebProxy proxy)
		{
			if (string.IsNullOrEmpty(url))
			{
				return null;
			}

			if (url.IndexOf("://", StringComparison.Ordinal) <= 0)
			{
				url = "https://" + url.Replace(",", ".");
			}

			try
			{
				using (var client = new WebClient())
				{
					if (proxy != null)
					{
						client.Proxy = proxy;
					}

					var response = client.UploadValues(url, postData);

					var enc = new UTF8Encoding();
					var outp = enc.GetString(response);

					return outp;
				}
			}

			catch (WebException e)
			{
				Debug.WriteLine(e.Message);
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
			}

			return null;
		}

		public string RequestDeleteToUrl(string url, WebProxy proxy)
		{
			if (string.IsNullOrEmpty(url))
			{
				return null;
			}

			if (url.IndexOf("://", StringComparison.Ordinal) <= 0)
			{
				url = "https://" + url.Replace(",", ".");
			}

			try
			{
				var request = WebRequest.Create(url);

				if (proxy != null)
				{
					request.Proxy = proxy;
				}

				request.Method = "DELETE";

				var str = "";
				var resp = request.GetResponse();
				var encode = Encoding.GetEncoding("utf-8");

				using (var receiveStream = resp.GetResponseStream())
				{
					if (receiveStream == null) return str;

					var readStream = new StreamReader(receiveStream, encode);
					var read = new char[256];
					var count = readStream.Read(read, 0, 256);

					while (count > 0)
					{
						str = str + new string(read, 0, count);
						count = readStream.Read(read, 0, 256);
					}

					readStream.Close();
				}

				return str;
			}

			catch (WebException e)
			{
				Debug.WriteLine(e.Message);
			}

			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
			}

			return null;
		}

		public string RequestGetToUrl(string url, WebProxy proxy)
		{
			if (string.IsNullOrEmpty(url))
			{
				return null;
			}

			if (url.IndexOf("://", StringComparison.Ordinal) <= 0)
			{
				url = "https://" + url.Replace(",", ".");
			}

			try
			{
				using (var client = new WebClient())
				{
					if (proxy != null)
					{
						client.Proxy = proxy;
					}

					var enc = new UTF8Encoding();
					var response = client.DownloadData(url);

					var outp = enc.GetString(response);
					return outp;
				}
			}

			catch (WebException e)
			{
				Debug.WriteLine(e.Message);
			}

			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
			}

			return null;
		}
	}
}