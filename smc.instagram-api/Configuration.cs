﻿using System.Net;

namespace smc.api.instagram
{
	public class Configuration
	{
		public const string AuthUrl = "https://api.instagram.com/oauth/authorize/";
		public const string ApiBaseUrl = "https://api.instagram.com/v1/";
		public const string TokenRetrievalUrl = "https://api.instagram.com/oauth/access_token";

		public string ClientId { get; set; }
		public string ClientSecret { get; set; }
		public string ReturnUrl { get; set; }
		
		public string WebProxy { get; set; }

		public WebProxy Proxy
		{
			get
			{
				if (string.IsNullOrEmpty(WebProxy)) return null;

				var pcs = WebProxy.Split(':');
				return new WebProxy(pcs[0], int.Parse(pcs[1]));
			}
		}

		public Configuration()
		{
		}

		public Configuration(string clientId, string clientSecret, string returnUrl, string proxy = null)
		{
			ClientId = clientId;
			ClientSecret = clientSecret;
			ReturnUrl = returnUrl;
			WebProxy = proxy;
		}
	}
}
