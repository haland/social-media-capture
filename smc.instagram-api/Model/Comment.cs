﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Comment : InstagramBaseObject
	{
		[JsonProperty("created_time")]
		public double CreatedTime;

		[JsonProperty("text")]
		public string Text;

		[JsonProperty("id")]
		public string Id;

		[JsonProperty("from")]
		public User From;

		public bool Owncomment = false;
	}
}