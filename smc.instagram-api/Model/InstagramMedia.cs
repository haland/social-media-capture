﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class InstagramMedia : InstagramBaseObject
	{
		[JsonProperty("id")]
		public string Id;

		[JsonProperty("type")]
		public string Type;

		[JsonProperty("link")]
		public string Link;

		[JsonProperty("created_time")]
		public double CreatedTime;

		[JsonProperty("location")]
		public Location Location;

		[JsonProperty("filter")]
		public string Filter;

		[JsonProperty("tags")]
		public string[] Tags;

		[JsonProperty("user")]
		public User User;

		[JsonProperty("caption")]
		public Caption Caption;

		[JsonProperty("comments")]
		public CommentList Comments;

		[JsonProperty("likes")]
		public LikesList Likes;

		[JsonProperty("images")]
		public ImagesList Images;

		public bool UserHasLiked;
	}
}