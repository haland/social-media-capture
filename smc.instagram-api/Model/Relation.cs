﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Relation : InstagramBaseObject
	{
		[JsonProperty("outgoing_status")]
		public string OutgoingStatus;

		[JsonProperty("incoming_status")]
		public string IncomingStatus;
	}
}