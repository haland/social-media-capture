﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class InstagramResponse<T>
	{
		[JsonProperty("pagination")]
		public Pagination Pagination;

		[JsonProperty("meta")]
		public Metadata Meta;

		[JsonProperty("data")]
		public T Data;
	}
}
