﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class ImagesList : InstagramBaseObject
	{
		[JsonProperty("low_resolution")]
		public ImageLink LowResolution;

		[JsonProperty("thumbnail")]
		public ImageLink Thumbnail;

		[JsonProperty("standard_resolution")]
		public ImageLink StandardResolution;
	}
}