﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class ImageLink : InstagramBaseObject
	{
		[JsonProperty("url")]
		public string Url;

		[JsonProperty("width")]
		public int Width;

		[JsonProperty("height")]
		public int Height;
	}
}