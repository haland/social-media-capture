﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Metadata : InstagramBaseObject
	{
		[JsonProperty("code")]
		public string Code;
	}
}