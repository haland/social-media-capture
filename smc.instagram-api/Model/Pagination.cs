﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Pagination : InstagramBaseObject
	{
		[JsonProperty("next_url")]
		public string NextUrl;

		[JsonProperty("next_max_id")]
		public string NextMaxId;

		[JsonProperty("next_min_id")]
		public string NextMinId;

		[JsonProperty("next_max_like_id")]
		public string NextMaxLikeId;
	}
}