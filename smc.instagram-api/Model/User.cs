﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class User : InstagramBaseObject, IEquatable<User>
	{
		[JsonProperty("id")]
		public string Id;

		[JsonProperty("username")]
		public string Username;

		[JsonProperty("full_name")]
		public string FullName;

		[JsonProperty("profile_picture")]
		public string ProfilePicture;

		[JsonProperty("first_name")]
		public string FirstName;

		[JsonProperty("last_name")]
		public string LastName;

		[JsonProperty("bio")]
		public string Bio;

		[JsonProperty("website")]
		public string Website;

		[JsonProperty("type")]
		public string Type;

		[JsonProperty("counts")]
		public Counts Counts;

		[JsonProperty("is_following")]
		public bool IsFollowing;

		[JsonProperty("is_followed")]
		public bool IsFollowed;

		[JsonProperty("is_self")]
		public bool IsSelf;

		public bool Equals(User other)
		{
			if (ReferenceEquals(other, null)) return false;
			return ReferenceEquals(this, other) || Id.Equals(other.Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(obj, null)) return false;
			if (ReferenceEquals(this, obj)) return true;

			var user = obj as User;
			return user != null && Id.Equals(user.Id);
		}

		public override int GetHashCode()
		{
			var hashProductName = Id == null ? 0 : int.Parse(Id);
			return hashProductName;
		}
	}
}