﻿using System;
using smc.api.instagram.Helpers;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class AccessToken : InstagramBaseObject
	{
		private readonly string _accessToken;
		public User User;

		public AccessToken()
		{
		}

		public AccessToken(string json)
		{
			var tk = Deserialize(json);
			_accessToken = tk._accessToken;
			User = tk.User;
		}

		public string GetJson()
		{
			return Serialize(this);
		}

		public static string Serialize(AccessToken token)
		{
			var json = Base.SerializeObject(token);
			return json;
		}

		public static AccessToken Deserialize(string json)
		{
			var token = Base.DeserializeObject<AccessToken>(json);
			return token;
		}
	}
}
