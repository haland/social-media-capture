﻿using System;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class InstagramBaseObject
	{
		protected InstagramApi InstagramApi => InstagramApi.GetInstance();
	}
}
