﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class LikesList : InstagramBaseObject
	{
		[JsonProperty("count")]
		public int Count;

		[JsonProperty("data")]
		public User[] Data;
	}
}