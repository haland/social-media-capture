﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Counts : InstagramBaseObject
	{
		[JsonProperty("media")]
		public int Media;

		[JsonProperty("follows")]
		public int Follows;

		[JsonProperty("followed_by")]
		public int FollowedBy;
	}
}
