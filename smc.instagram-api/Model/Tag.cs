﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Tag : InstagramBaseObject
	{
		[JsonProperty("name")]
		public string Name;

		[JsonProperty("media_count")]
		public int MediaCount;
	}
}