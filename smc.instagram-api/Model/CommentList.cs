﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class CommentList : InstagramBaseObject
	{
		[JsonProperty("count")]
		public int Count;

		[JsonProperty("data")]
		public Comment[] Data;
	}
}