﻿using System;
using Newtonsoft.Json;

namespace smc.api.instagram.Model
{
	[Serializable]
	public class Location : InstagramBaseObject
	{
		[JsonProperty("id")]
		public string Id;

		[JsonProperty("latitude")]
		public double Latitude;

		[JsonProperty("longitude")]
		public double Longitude;

		[JsonProperty("name")]
		public string Name;
	}
}
