﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using smc.api.instagram.Helpers;
using smc.api.instagram.Model;

namespace smc.api.instagram
{
	public class InstagramApi : Base
	{
		private static InstagramApi _sharedInstance;
		private static Configuration _sharedConfiguration;

		public Configuration Configuration
		{
			get { return _sharedConfiguration; }
			set { _sharedConfiguration = value; }
		}

		public static InstagramApi GetInstance(Configuration configuration, ICache cache)
		{
			lock (Threadlock)
			{
				if (_sharedInstance != null) return _sharedInstance;

				_sharedInstance = new InstagramApi { Configuration = configuration };
				Cache = cache;
			}

			return _sharedInstance;
		}

		public static InstagramApi GetInstance(Configuration configuration)
		{
			lock (Threadlock)
			{
				if (_sharedInstance != null) return _sharedInstance;
				_sharedInstance = new InstagramApi { Configuration = configuration };
			}

			return _sharedInstance;
		}

		public static InstagramApi GetInstance()
		{
			if (_sharedInstance != null) return _sharedInstance;

			if (_sharedConfiguration == null)
			{
				throw new ApplicationException("API Uninitialized");
			}

			_sharedInstance = new InstagramApi();
			return _sharedInstance;
		}

		#region Auth

		public string AuthGetUrl(string scope)
		{
			if (string.IsNullOrEmpty(scope))
			{
				scope = "basic";
			}

			return Configuration.AuthUrl + "?client_id=" + Configuration.ClientId + "&redirect_uri=" + Configuration.ReturnUrl + "&response_type=code&scope=" + scope;
		}

		public AccessToken AuthGetAccessToken(string code)
		{
			var json = RequestPostToUrl(Configuration.TokenRetrievalUrl,
				new NameValueCollection
				{
					{"client_id" , Configuration.ClientId},
					{"client_secret" , Configuration.ClientSecret},
					{"grant_type" , "authorization_code"},
					{"redirect_uri" , Configuration.ReturnUrl},
					{"code" , code}
				}, Configuration.Proxy);

			if (string.IsNullOrEmpty(json)) return null;

			var tk = AccessToken.Deserialize(json);
			return tk;
		}

		#endregion

		#region User

		public InstagramResponse<User> UserDetails(string userid, string accessToken)
		{
			if (userid == "self")
			{
				return CurrentUserDetails(accessToken);
			}

			var url = Configuration.ApiBaseUrl + "users/" + userid + "?access_token=" + accessToken;
			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "users/" + userid + "?client_id=" + Configuration.ClientId;
			}

			if (Cache != null && Cache.Exists(url))
			{
				return Cache.Get<InstagramResponse<User>>("users/" + userid);
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User>>(json);

			if (!string.IsNullOrEmpty(accessToken))
			{
				res.Data.IsFollowed = CurrentUserIsFollowing(res.Data.Id, accessToken);
			}

			Cache?.Add("users/" + userid, res, 600);

			return res;
		}

		public InstagramResponse<User[]> UsersSearch(string query, string count, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/search?access_token=" + accessToken;
			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "users/search?client_id=" + Configuration.ClientId;
			}

			if (Cache != null && Cache.Exists(url))
			{
				return Cache.Get<InstagramResponse<User[]>>(url);
			}

			if (!string.IsNullOrEmpty(query)) url = url + "&q=" + query;
			if (!string.IsNullOrEmpty(count)) url = url + "&count=" + count;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);

			Cache?.Add(url, res, 300);

			return res;
		}

		public User[] UsersPopular(string accessToken)
		{
			var media = MediaPopular(accessToken, true).Data;
			var users = UsersInMediaList(media);
			return users;
		}

		public InstagramResponse<InstagramMedia[]> UserRecentMedia(string userid, string minId, string maxId, string count, string minTimestamp, string maxTimestamp, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/" + userid + "/media/recent?access_token=" + accessToken;
			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "users/" + userid + "/media/recent?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(minId)) url = url + "&min_id=" + minId;
			if (!string.IsNullOrEmpty(maxId)) url = url + "&max_id=" + maxId;
			if (!string.IsNullOrEmpty(count)) url = url + "&count=" + count;
			if (!string.IsNullOrEmpty(minTimestamp)) url = url + "&min_timestamp=" + minTimestamp;
			if (!string.IsNullOrEmpty(maxTimestamp)) url = url + "&max_timestamp=" + maxTimestamp;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);
			return res;
		}

		public InstagramResponse<User> CurrentUserDetails(string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/self?access_token=" + accessToken;

			if (Cache != null && Cache.Exists("users/self/" + accessToken))
			{
				return Cache.Get<InstagramResponse<User>>("users/self/" + accessToken);
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User>>(json);
			res.Data.IsSelf = true;

			Cache?.Add("users/self/" + accessToken, res, 600);

			return res;
		}

		public InstagramResponse<InstagramMedia[]> CurrentUserRecentMedia(string minId, string maxId, string count, string minTimestamp, string maxTimestamp, string accessToken)
		{
			return UserRecentMedia("self", minId, maxId, count, minTimestamp, maxTimestamp, accessToken);
		}

		public InstagramResponse<InstagramMedia[]> CurrentUserFeed(string minId, string maxId, string count, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/self/feed?access_token=" + accessToken;

			if (!string.IsNullOrEmpty(minId)) url = url + "&min_id=" + minId;
			if (!string.IsNullOrEmpty(maxId)) url = url + "&max_id=" + maxId;
			if (!string.IsNullOrEmpty(count)) url = url + "&count=" + count;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);
			return res;
		}

		public InstagramResponse<InstagramMedia[]> CurrentUserLikedMedia(string maxLikeId, string count, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/self/media/liked?access_token=" + accessToken;

			if (!string.IsNullOrEmpty(maxLikeId)) url = url + "&max_like_id=" + maxLikeId;
			if (!string.IsNullOrEmpty(count)) url = url + "&count=" + count;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);
			return res;
		}

		public User[] UsersInMediaList(InstagramMedia[] media)
		{
			var users = new List<User>();
			foreach (var instagramMedia in media.Where(instagramMedia => !users.Contains(instagramMedia.User)))
			{
				users.Add(instagramMedia.User);
			}

			return users.ToArray();
		}

		#endregion

		#region Relationships
		public InstagramResponse<User[]> UserFollows(string userid, string accessToken, string maxUserId)
		{
			var url = Configuration.ApiBaseUrl + "users/" + userid + "/follows?access_token=" + accessToken;
			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "users/" + userid + "/follows?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(maxUserId)) url = url + "&cursor=" + maxUserId;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);
			return res;
		}

		public InstagramResponse<User[]> UserFollowedBy(string userid, string accessToken, string maxUserId)
		{
			var url = Configuration.ApiBaseUrl + "users/" + userid + "/followed-by?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "users/" + userid + "/followed-by?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(maxUserId)) url = url + "&cursor=" + maxUserId;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);
			return res;
		}

		public InstagramResponse<User[]> CurrentUserFollows(string accessToken, string maxUserId)
		{
			var url = Configuration.ApiBaseUrl + "users/self/follows?access_token=" + accessToken;

			if (!string.IsNullOrEmpty(maxUserId)) url = url + "&cursor=" + maxUserId;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);
			return res;
		}

		public InstagramResponse<User[]> CurrentUserFollowedBy(string accessToken, string maxUserId)
		{
			var url = Configuration.ApiBaseUrl + "users/self/followed-by?access_token=" + accessToken;

			if (!string.IsNullOrEmpty(maxUserId)) url = url + "&cursor=" + maxUserId;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);
			return res;
		}

		public InstagramResponse<User[]> CurrentUserRequestedBy(string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/self/requested-by?access_token=" + accessToken;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);
			return res;
		}

		public InstagramResponse<Relation> CurrentUserRelationshipWith(string recipientUserid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/" + recipientUserid + "/relationship?access_token=" + accessToken;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<Relation>>(json);
			return res;
		}

		public void CurrentUserFollow(string userid, string[] recipientUserids, string accessToken)
		{
			foreach (var recipientUserid in recipientUserids)
			{
				CurrentUserFollow(userid, recipientUserid, accessToken);
			}
		}

		public bool CurrentUserFollow(string userid, string recipientUserid, string accessToken)
		{
			if (Cache == null)
			{
				return CurrentUserSetRelationship(userid, recipientUserid, "follow", accessToken).Meta.Code == "200";
			}

			Cache.Remove(userid + "/follows");
			Cache.Remove("users/self/" + accessToken);

			return CurrentUserSetRelationship(userid, recipientUserid, "follow", accessToken).Meta.Code == "200";
		}

		public bool CurrentUserFollowToggle(string userid, string recipientUserid, string accessToken)
		{
			if (Cache != null)
			{
				Cache.Remove("self" + "/follows");
				Cache.Remove(userid + "/follows");
				Cache.Remove("users/" + recipientUserid);
				Cache.Remove("users/self/" + accessToken);
			}

			if (CurrentUserIsFollowing(recipientUserid, accessToken))
			{
				return CurrentUserSetRelationship(userid, recipientUserid, "unfollow", accessToken).Meta.Code == "200";
			}

			return CurrentUserSetRelationship(userid, recipientUserid, "follow", accessToken).Meta.Code == "200";
		}

		public void CurrentUserUnfollow(string userid, string[] recipientUserids, string accessToken)
		{
			foreach (var recipientUserid in recipientUserids)
			{
				CurrentUserUnfollow(userid, recipientUserid, accessToken);
			}

			Cache?.Remove(userid + "/follows");
		}

		public bool CurrentUserUnfollow(string userid, string recipientUserid, string accessToken)
		{
			if (Cache == null)
			{
				return CurrentUserSetRelationship(userid, recipientUserid, "unfollow", accessToken).Meta.Code == "200";
			}

			Cache.Remove(userid + "/follows");
			Cache.Remove("users/self/" + accessToken);

			return CurrentUserSetRelationship(userid, recipientUserid, "unfollow", accessToken).Meta.Code == "200";
		}

		public bool CurrentUserBlock(string userid, string recipientUserid, string accessToken)
		{
			return CurrentUserSetRelationship(userid, recipientUserid, "block", accessToken).Meta.Code == "200";
		}

		public void CurrentUserApprove(string userid, string[] recipientUserids, string accessToken)
		{
			foreach (var recipientUserid in recipientUserids)
			{
				CurrentUserApprove(userid, recipientUserid, accessToken);
			}
		}

		public bool CurrentUserApprove(string userid, string recipientUserid, string accessToken)
		{
			return CurrentUserSetRelationship(userid, recipientUserid, "approve", accessToken).Meta.Code == "200";
		}

		public bool CurrentUserDeny(string userid, string recipientUserid, string accessToken)
		{
			return CurrentUserSetRelationship(userid, recipientUserid, "deny", accessToken).Meta.Code == "200";
		}

		public bool CurrentUserUnblock(string userid, string recipientUserid, string accessToken)
		{
			return CurrentUserSetRelationship(userid, recipientUserid, "unblock", accessToken).Meta.Code == "200";
		}

		public InstagramResponse<Relation> CurrentUserSetRelationship(string userid, string recipientUserid, string relationshipKey, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "users/" + recipientUserid + "/relationship?access_token=" + accessToken;
			url = url + "&action=" + relationshipKey;

			var json = RequestPostToUrl(url, new NameValueCollection { { "action", relationshipKey } }, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return new InstagramResponse<Relation> { Meta = new Metadata { Code = "400" } };
			}

			var res = DeserializeObject<InstagramResponse<Relation>>(json);

			if (Cache == null) return res;

			Cache.Remove("users/self/" + accessToken);
			Cache.Remove(userid + "/follows");
			Cache.Remove("users/" + recipientUserid);

			return res;
		}

		public bool CurrentUserIsFollowing(string recipientUserid, string accessToken)
		{
			var r = CurrentUserRelationshipWith(recipientUserid, accessToken).Data;
			return r.OutgoingStatus == "follows";
		}

		public bool CurrentUserIsFollowedBy(string recipientUserid, string accessToken)
		{
			var r = CurrentUserRelationshipWith(recipientUserid, accessToken).Data;
			return r.IncomingStatus == "followed_by";
		}

		#endregion

		#region Media

		public InstagramResponse<InstagramMedia> MediaDetails(string mediaid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "?access_token=" + accessToken;
			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "media/" + mediaid + "?client_id=" + Configuration.ClientId;
			}

			if (Cache != null && Cache.Exists("media/" + mediaid))
			{
				return Cache.Get<InstagramResponse<InstagramMedia>>("media/" + mediaid);
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia>>(json);
			Cache?.Add("media/" + mediaid, res, 60);

			return res;
		}

		public InstagramResponse<InstagramMedia[]> MediaSearch(string lat, string lng, string distance, string minTimestamp, string maxTimestamp, string accessToken)
		{
			if (!string.IsNullOrEmpty(lat) && string.IsNullOrEmpty(lng) || !string.IsNullOrEmpty(lng) && string.IsNullOrEmpty(lat))
			{
				throw new ArgumentException("if lat or lng are specified, both are required.");
			}

			var url = Configuration.ApiBaseUrl + "media/search?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "media/search?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(lat)) url = url + "&lat=" + lat;
			if (!string.IsNullOrEmpty(lng)) url = url + "&lng=" + lng;
			if (!string.IsNullOrEmpty(distance)) url = url + "&distance=" + distance;
			if (!string.IsNullOrEmpty(minTimestamp)) url = url + "&min_timestamp=" + minTimestamp;
			if (!string.IsNullOrEmpty(maxTimestamp)) url = url + "&max_timestamp=" + maxTimestamp;

			if (Cache != null && Cache.Exists(url))
			{
				return Cache.Get<InstagramResponse<InstagramMedia[]>>(url);
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);

			Cache?.Add(url, res, 60);

			return res;
		}

		public InstagramResponse<InstagramMedia[]> MediaPopular(string accessToken, bool usecache)
		{
			var url = Configuration.ApiBaseUrl + "media/popular?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "media/popular?client_id=" + Configuration.ClientId;
			}

			if (Cache != null && usecache && Cache.Exists("media/popular"))
			{
				return Cache.Get<InstagramResponse<InstagramMedia[]>>("media/popular");
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);

			Cache?.Add("media/popular", res, 600);

			return res;
		}

		#endregion

		#region Comments

		public InstagramResponse<Comment[]> Comments(string mediaid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "/comments?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "media/" + mediaid + "/comments?client_id=" + Configuration.ClientId;
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<Comment[]>>(json);
			return res;
		}

		public void CommentAdd(string[] mediaids, string text, string accessToken)
		{
			foreach (var mediaid in mediaids)
			{
				CommentAdd(mediaid, text, accessToken);
			}
		}

		public bool CommentAdd(string mediaid, string text, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "/comments?access_token=" + accessToken;
			var post = new NameValueCollection
						{
							{"Text",text},
							{"access_token", accessToken}
						};

			var json = RequestPostToUrl(url, post, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return true;
			}

			var res = DeserializeObject<InstagramResponse<Comment>>(json);

			Cache?.Remove("media/" + mediaid);

			return res.Meta.Code == "200";
		}

		public bool CommentDelete(string mediaid, string commentid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "/comments/" + commentid + "?access_token=" + accessToken;

			var json = RequestDeleteToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return false;
			}

			var res = DeserializeObject<InstagramResponse<Comment>>(json);

			Cache?.Remove("media/" + mediaid);

			return res.Meta.Code == "200";
		}

		#endregion

		#region Likes

		public InstagramResponse<User[]> Likes(string mediaid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "/likes?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "media/" + mediaid + "/likes?client_id=" + Configuration.ClientId;
			}

			if (Cache != null && Cache.Exists("media/" + mediaid + "/likes"))
			{
				return Cache.Get<InstagramResponse<User[]>>("media/" + mediaid + "/likes");
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);

			Cache?.Add("media/" + mediaid + "/likes", res, 60);

			return res;
		}

		public void LikeAdd(string[] mediaids, string accessToken)
		{
			foreach (var mediaid in mediaids)
			{
				LikeAdd(mediaid, null, accessToken);
			}
		}

		public bool LikeAdd(string mediaid, string userid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "/likes?access_token=" + accessToken;
			var post = new NameValueCollection
					   {
						   {"access_token", accessToken}
					   };

			var json = RequestPostToUrl(url, post, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return true;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);

			if (Cache == null) return res.Meta.Code == "200";

			Cache.Remove("media/" + mediaid);
			Cache.Remove("media/popular");
			Cache.Remove("media/" + mediaid + "/likes");
			Cache.Remove("users/self/" + accessToken);
			Cache.Remove("users/" + userid);

			if (string.IsNullOrEmpty(userid)) return res.Meta.Code == "200";

			Cache.Remove("users/" + userid + "/media/recent");
			Cache.Remove("users/self/feed?access_token=" + accessToken);

			return res.Meta.Code == "200";
		}

		public void LikeDelete(string[] mediaids, string accessToken)
		{
			foreach (var mediaid in mediaids)
			{
				LikeDelete(mediaid, null, accessToken);
			}
		}

		public bool LikeDelete(string mediaid, string userid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "media/" + mediaid + "/likes?access_token=" + accessToken;

			var json = RequestDeleteToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return true;
			}

			var res = DeserializeObject<InstagramResponse<User[]>>(json);

			if (Cache == null) return res.Meta.Code == "200";

			Cache.Remove("media/popular");
			Cache.Remove("media/" + mediaid);
			Cache.Remove("media/" + mediaid + "/likes");
			Cache.Remove("users/self/" + accessToken);
			Cache.Remove("users/" + userid);

			if (string.IsNullOrEmpty(userid)) return res.Meta.Code == "200";

			Cache.Remove("users/" + userid + "/media/recent");
			Cache.Remove("users/self/feed?access_token=" + accessToken);

			return res.Meta.Code == "200";
		}

		public bool LikeToggle(string mediaid, string userid, string accessToken)
		{
			var media = MediaDetails(mediaid, accessToken).Data;
			return media.UserHasLiked ? LikeDelete(mediaid, userid, accessToken) : LikeAdd(mediaid, userid, accessToken);
		}

		public bool UserIsLiking(string mediaid, string userid, string accessToken)
		{
			var userlinking = Likes(mediaid, accessToken).Data;

			return userlinking.Any(user => user.Id.ToString().Equals(userid));
		}

		#endregion

		#region Tags

		public InstagramResponse<Tag> TagDetails(string tagname, string accessToken)
		{
			if (tagname.Contains("#"))
			{
				tagname = tagname.Replace("#", "");
			}

			var url = Configuration.ApiBaseUrl + "tags/" + tagname + "?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "tags/" + tagname + "?client_id=" + Configuration.ClientId;
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<Tag>>(json);
			return res;
		}

		public InstagramResponse<Tag[]> TagSearch(string query, string accessToken)
		{
			if (query.Contains("#"))
			{
				query = query.Replace("#", "");
			}

			var url = Configuration.ApiBaseUrl + "tags/search?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "tags/search?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(query)) url = url + "&q=" + query;

			if (Cache != null && Cache.Exists(url))
			{
				return Cache.Get<InstagramResponse<Tag[]>>(url);
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<Tag[]>>(json);

			Cache?.Add(url, res, 300);

			return res;
		}

		public Tag[] TagPopular(string accessToken)
		{
			var pop = MediaPopular(accessToken, true).Data;
			return TagsInMediaList(pop);
		}

		public InstagramResponse<InstagramMedia[]> TagMedia(string tagname, string minId, string maxId, string accessToken = null)
		{
			if (tagname.Contains("#"))
			{
				tagname = tagname.Replace("#", "");
			}

			var url = Configuration.ApiBaseUrl + "tags/" + tagname + "/media/recent?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "tags/" + tagname + "/media/recent?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(minId)) url = url + "&min_tag_id=" + minId;
			if (!string.IsNullOrEmpty(maxId)) url = url + "&max_tag_id=" + maxId;

			if (Cache != null && Cache.Exists(url))
			{
				return Cache.Get<InstagramResponse<InstagramMedia[]>>(url);
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);

			Cache?.Add(url, res, 300);

			return res;
		}

		public static Tag[] TagsInMediaList(InstagramMedia[] media)
		{
			var t = new List<string>();
			foreach (var tag in from instagramMedia in media from tag in instagramMedia.Tags where !t.Contains(tag) select tag)
			{
				t.Add(tag);
			}

			return TagsFromStrings(t.ToArray());
		}

		public static Tag[] TagsFromStrings(string[] tags)
		{
			var taglist = new List<Tag>(tags.Length);
			taglist.AddRange(tags.Select(s => new Tag
			{
				MediaCount = 0,
				Name = s
			}));

			return taglist.ToArray();
		}

		#endregion

		#region Locations
		public Location LocationDetails(string locationid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "locations/" + locationid + "?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "locations/" + locationid + "?client_id=" + Configuration.ClientId;
			}

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			var res = DeserializeObject<InstagramResponse<Location>>(json);
			return res.Data;
		}

		public InstagramMedia[] LocationMedia(string locationid, string minId, string maxId, string minTimestamp, string maxTimestamp, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "locations/" + locationid + "/media/recent?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "locations/" + locationid + "/media/recent?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(minId)) url = url + "&min_id=" + minId;
			if (!string.IsNullOrEmpty(maxId)) url = url + "&max_id=" + maxId;
			if (!string.IsNullOrEmpty(minTimestamp)) url = url + "&min_timestamp=" + minTimestamp;
			if (!string.IsNullOrEmpty(maxTimestamp)) url = url + "&max_timestamp=" + maxTimestamp;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return new InstagramMedia[0];
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);
			return res.Data;
		}

		public Location[] LocationSearch(string lat, string lng, string foursquareId, string foursquareV2Id, string distance, string accessToken)
		{
			if (!string.IsNullOrEmpty(lat) && string.IsNullOrEmpty(lng) || !string.IsNullOrEmpty(lng) && string.IsNullOrEmpty(lat))
			{
				throw new ArgumentException("if lat or lng are specified, both are required.");
			}

			var url = Configuration.ApiBaseUrl + "locations/search?access_token=" + accessToken;

			if (string.IsNullOrEmpty(accessToken))
			{
				url = Configuration.ApiBaseUrl + "locations/search?client_id=" + Configuration.ClientId;
			}

			if (!string.IsNullOrEmpty(lat)) url = url + "&lat=" + lat;
			if (!string.IsNullOrEmpty(lng)) url = url + "&lng=" + lng;
			if (!string.IsNullOrEmpty(foursquareId)) url = url + "&foursquare_id=" + foursquareId;
			if (!string.IsNullOrEmpty(foursquareV2Id)) url = url + "&foursquare_v2_id=" + foursquareV2Id;
			if (!string.IsNullOrEmpty(distance)) url = url + "&distance=" + distance;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return new Location[0];
			}

			var res = DeserializeObject<InstagramResponse<Location[]>>(json);
			return res.Data;
		}

		#endregion

		#region Geography

		public InstagramMedia[] GeographyMedia(string geographyid, string accessToken)
		{
			var url = Configuration.ApiBaseUrl + "geographies/" + geographyid + "/media/recent?access_token=" + accessToken;

			var json = RequestGetToUrl(url, Configuration.Proxy);
			if (string.IsNullOrEmpty(json))
			{
				return new InstagramMedia[0];
			}

			var res = DeserializeObject<InstagramResponse<InstagramMedia[]>>(json);
			return res.Data;
		}

		#endregion

		#region OEmbed

		public static string GenerateOEmbedPostHtml(InstagramMedia media)
		{
			return
				"<blockquote class=\"instagram-media\" data-instgrm-captioned data-instgrm-version=\"6\" style=\"background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);\">" +
					"<div style=\"padding:8px;\">" +
						"<p style=\" margin:8px 0 0 0; padding:0 4px;\">" +
							$"<a href=\"{media.Link}\" style=\" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;\" target = \"_blank\" ></a>" +
						"</p>" +
					"</div>" +
				"</blockquote>";
		}

		#endregion
	}
}