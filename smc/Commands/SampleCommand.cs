﻿using ManyConsole;
using smc.Controller;

namespace smc.Commands
{
	public class SampleConsole : ConsoleCommand
	{
		private string _hashtag;

		private bool _sampleTwitter;
		private bool _sampleFacebook;
		private bool _sampleInstagram;

		public SampleConsole()
		{
			IsCommand("sample", "sample data from social media");
			HasRequiredOption("hashtag|h=", "hashtag to listen for", h => _hashtag = h);
			HasOption("twitter|t", "sample data from Twitter", t => _sampleTwitter = true);
			HasOption("facebook|f", "sample data from Facebook", f => _sampleFacebook = true);
			HasOption("instagram|i", "sample data from Instagram", i => _sampleInstagram = true);

		}

		public override int Run(string[] remainingArguments)
		{
			if (!_sampleTwitter && !_sampleFacebook && !_sampleInstagram)
			{
				new SampleController(_hashtag, true, true, true);
			}
			else
			{
				new SampleController(_hashtag, _sampleTwitter, _sampleFacebook, _sampleInstagram);
			}

			return 0;
		}
	}
}