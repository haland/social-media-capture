﻿using System;
using System.Collections.Generic;
using ManyConsole;

namespace smc
{
	class Program
	{
		static int Main(string[] args)
		{
			return ConsoleCommandDispatcher.DispatchCommand(GetCommands(), args, Console.Out);
		}

		private static IEnumerable<ConsoleCommand> GetCommands()
		{
			return ConsoleCommandDispatcher.FindCommandsInSameAssemblyAs(typeof (Program));
		}
	}
}
