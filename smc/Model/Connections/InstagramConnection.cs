﻿using System;
using System.Collections.Generic;
using System.Threading;
using smc.api.instagram;
using Configuration = smc.api.instagram.Configuration;

namespace smc.Model.Connections
{
	public class InstagramConnection : Connection
	{
		private const int SleepTime = 5000000;

		private readonly InstagramApi _instagramApi;

		public InstagramConnection()
		{
			var config = new Configuration {
				ClientId = Settings["InstagramClientId"],
				ClientSecret = Settings["InstagramClientSecret"],
				ReturnUrl = Settings["InstagramReturnUrl"]
			};

			_instagramApi = InstagramApi.GetInstance(config);
		}

		public override void Fetch(string hashtag, Action<IEnumerable<object>> action)
		{
			var result = _instagramApi.TagMedia(hashtag, null, null);
			action(result.Data);
		}

		public override void Sample(string hashtag, Action<object> action)
		{
			string maxId = null;

			while (true)
			{
				var result = _instagramApi.TagMedia(hashtag, null, maxId);

				if (result?.Data == null) continue;

				foreach (var post in result.Data)
				{
					action(post);
				}

				maxId = result.Pagination.NextMaxId;

				Thread.Sleep(SleepTime);
			}
		}
	}
}