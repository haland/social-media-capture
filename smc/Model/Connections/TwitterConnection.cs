﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tweetinvi;
using Tweetinvi.Core.Exceptions;
using Tweetinvi.Core.Parameters;
using Stream = Tweetinvi.Stream;

namespace smc.Model.Connections
{
	public class TwitterConnection : Connection
	{
		private const string TwitterConsumerKey = "TwitterConsumerKey";
		private const string TwitterConsumerSecret = "TwitterConsumerSecret";
		private const string TwitterAccessToken = "TwitterAccessToken";
		private const string TwitterAccessTokenSecret = "TwitterAccessTokenSecret";

		private const int MaxCount = 100;

		public TwitterConnection()
		{
			if (Settings[TwitterConsumerKey] == null)
			{
				Console.WriteLine($"{TwitterConsumerKey} needs to be defined in App.Secrets.config");
			}

			if (Settings[TwitterConsumerSecret] == null)
			{
				Console.WriteLine($"{TwitterConsumerSecret} needs to be defined in App.Secrets.config");
			}

			if (Settings[TwitterAccessToken] == null)
			{
				Console.WriteLine($"{TwitterAccessToken} needs to be defined in App.Secrets.config");
			}

			if (Settings[TwitterAccessTokenSecret] == null)
			{
				Console.WriteLine($"{TwitterAccessTokenSecret} needs to be defined in App.Secrets.config");
			}

			if (Settings[TwitterConsumerKey] != null && Settings[TwitterConsumerSecret] != null &&
			    Settings[TwitterAccessToken] != null && Settings[TwitterAccessTokenSecret] != null)
			{
				Auth.SetUserCredentials(Settings[TwitterConsumerKey], Settings[TwitterConsumerSecret], Settings[TwitterAccessToken],
					Settings[TwitterAccessTokenSecret]);
			}
		}

		public override void Fetch(string hashtag, Action<IEnumerable<object>> action)
		{
			var count = MaxCount;
			var maxId = long.MaxValue;

			while (count >= MaxCount)
			{
				var parameters = new TweetSearchParameters(hashtag) { MaxId = maxId, MaximumNumberOfResults = MaxCount };
				try
				{
					var tweets = Search.SearchTweets(parameters).ToList();

					count = tweets.Count;
					maxId = tweets.OrderBy(tweet => tweet.Id).First().Id;

					action(tweets);
				}
				catch (TwitterNullCredentialsException)
				{
					Console.WriteLine("Could not authenticate towards Twitter. Something wrong with your credentials?");
					return;
				}
			}
		}

		public override void Sample(string hashtag, Action<object> action)
		{
			var stream = Stream.CreateFilteredStream();
			stream.AddTrack(hashtag);

			stream.MatchingTweetReceived += (sender, args) =>
			{
				action(args.Tweet);
			};
		}
	}
}