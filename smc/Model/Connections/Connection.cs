﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

namespace smc.Model.Connections
{
	public abstract class Connection
	{
		/// <summary>
		/// Local reference to the application settings
		/// </summary>
		protected static NameValueCollection Settings = ConfigurationManager.AppSettings;

		/// <summary>
		/// Get all media posts related to a hashtag
		/// </summary>
		/// <param name="hashtag">Hashtag to search for</param>
		/// <param name="action">Action to perform on the result</param>
		/// <returns></returns>
		public abstract void Fetch(string hashtag, Action<IEnumerable<object>> action);

		/// <summary>
		/// Listen for new media posts containing the given hashtag
		/// </summary>
		/// <param name="hashtag">Hashtag to filter for</param>
		/// <param name="action">Action to perform on the result</param>
		/// <returns></returns>
		public abstract void Sample(string hashtag, Action<object> action);
	}
}