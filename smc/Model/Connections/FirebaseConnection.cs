﻿using System.Collections.Specialized;
using System.Configuration;
using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;

namespace smc.Model.Connections
{
	public class FirebaseConnection
	{
		private static readonly NameValueCollection Settings = ConfigurationManager.AppSettings;

		private readonly string _mediaNode;
		private readonly IFirebaseClient _client;

		public FirebaseConnection()
		{
			_mediaNode = Settings["FirebaseNode"] + "/";

			_client = new FirebaseClient(new FirebaseConfig
			{
				BasePath = Settings["FirebaseUrl"],
                AuthSecret = Settings["FirebaseSecret"]
            });
		}

		public void Save(Media media)
		{
			var response = _client.Get(_mediaNode + media).ResultAs<Media>();

			if (response != null)
			{
				_client.UpdateAsync(_mediaNode + media, media);
			}
			else
			{
				_client.SetAsync(_mediaNode + media, media);
			}
		}
	}
}