﻿namespace smc.Model
{
	public class Media
	{
		public string Id { get; set; }
		public string Url { get; set; }
		public string Html { get; set; }
		public long Timestamp { get; set; }
		public string Type { get; set; }

		public override string ToString()
		{
			return Timestamp + "_" + Id + "_" + Type;
		}
	}
}
