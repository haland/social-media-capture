﻿using System.Collections.Concurrent;
using smc.Model.Connections;

namespace smc.Model.Services
{
	public class FacebookService : Service
	{
		public FacebookService(string hashtag, BlockingCollection<Media> channel) : base(hashtag, channel)
		{
			Connection = new FacebookConnection();
		}

		protected override void Live()
		{
			throw new System.NotImplementedException();
		}

		protected override void History()
		{
			throw new System.NotImplementedException();
		}
	}
}