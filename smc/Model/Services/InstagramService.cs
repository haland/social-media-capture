﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using smc.api.instagram;
using smc.api.instagram.Model;
using smc.Model.Connections;

namespace smc.Model.Services
{
	public class InstagramService : Service
	{
		public InstagramService(string hashtag, BlockingCollection<Media> channel) : base(hashtag, channel)
		{
			Connection = new InstagramConnection();
		}

		protected override void Live()
		{
			Connection.Sample(Hashtag, @object =>
			{
				var post = @object as InstagramMedia;

				if (post == null) return;

				Channel.Add(CreateMedia(post));
			});
		}

		protected override void History()
		{
			while (true)
			{
				Thread.Sleep(SleepTime);
			}
		}

		private static Media CreateMedia(InstagramMedia post)
		{
			return new Media
			{
				Type = MediaType.Instagram.ToString(),
				Id = post.Id,
				Url = post.Link,
				Timestamp = DateTimeOffset.FromUnixTimeSeconds((long)post.CreatedTime).Ticks,
				Html = InstagramApi.GenerateOEmbedPostHtml(post)
			};
		}
	}
}