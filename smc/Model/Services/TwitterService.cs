﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using smc.Model.Connections;
using Tweetinvi;
using Tweetinvi.Core.Interfaces;

namespace smc.Model.Services
{
	public class TwitterService : Service
	{
		public TwitterService(string hashtag, BlockingCollection<Media> channel) : base(hashtag, channel)
		{
			Connection = new TwitterConnection();
		}

		protected override void Live()
		{
			Connection.Sample(Hashtag, @object =>
			{
				var tweet = @object as ITweet;
				if (tweet != null)
				{
					Channel.Add(CreateMedia(tweet));
				}
			});
		}

		protected override void History()
		{
			while (true)
			{
				Connection.Fetch(Hashtag, @objects =>
				{
					var tweets = @objects.Select(x => x as ITweet);
					foreach (var tweet in tweets.Where(tweet => tweet != null))
					{
						Channel.Add(CreateMedia(tweet));
					}
				});

				Thread.Sleep(SleepTime);
			}
		}

		private static Media CreateMedia(ITweet tweet)
		{
			return new Media
			{
				Id = tweet.IdStr,
				Url = tweet.CreatedBy.Url + "/status/" + tweet.IdStr,
				Html = Tweet.GenerateOEmbedTweet(tweet).HTML,
				Type = MediaType.Twitter.ToString(),
				Timestamp = tweet.CreatedAt.Ticks
			};
		}
	}
}