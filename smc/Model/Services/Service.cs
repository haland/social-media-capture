﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using smc.Model.Connections;

namespace smc.Model.Services
{
	public abstract class Service
	{
		protected const int SleepTime = 60*60*1000;

		protected readonly string Hashtag;
		protected readonly BlockingCollection<Media> Channel;

		protected Connection Connection;

		protected Service(string hashtag, BlockingCollection<Media> channel)
		{
			Hashtag = hashtag;
			Channel = channel;
		}

		public void Start()
		{
			Task.Factory.StartNew(Live);
			Task.Factory.StartNew(History);
		}
		
		protected abstract void Live();
		protected abstract void History();

		protected static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
		{
			// Unix timestamp is seconds past epoch
			var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();

			return dtDateTime;
		}
	}
}