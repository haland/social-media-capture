﻿using System;
using System.Collections.Concurrent;
using smc.Model;
using smc.Model.Connections;
using smc.Model.Services;

namespace smc.Controller
{
	internal class SampleController
	{
		private readonly BlockingCollection<Media> _channel;
		private readonly FirebaseConnection _connection;

		public SampleController(string hashtag, bool sampleTwitter, bool sampleFacebook, bool sampleInstagram)
		{
			Console.WriteLine($"starting to listen for #{hashtag}");

			_connection = new FirebaseConnection();
			_channel = new BlockingCollection<Media>(10000);

			if (sampleTwitter)
			{
				var twitterService = new TwitterService(hashtag, _channel);
				twitterService.Start();

				Console.WriteLine("- sampling from Twitter");
			}

			if (sampleFacebook)
			{
				//TODO Implement a facebook service for sampling data
				Console.WriteLine("- sampling from Facebook");
			}

			if (sampleInstagram)
			{
				var instagramService = new InstagramService(hashtag, _channel);
				instagramService.Start();

				Console.WriteLine("- sampling from Instagram");
			}
			
			Process();
		}

		private void Process()
		{
			while (true)
			{
				_connection.Save(_channel.Take());
			}
		}
	}
}
